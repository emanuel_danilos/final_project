package com.mycompany.app.steps;

import com.mycompany.app.pages.AuthenticationPage;
import com.mycompany.app.pages.HomePage;
import com.mycompany.app.pages.MyAccountPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class AuthenticationStepdefs {

    HomePage homePage = new HomePage();
    AuthenticationPage authenticationPage = new AuthenticationPage();
    MyAccountPage myAccountPage = new MyAccountPage();

    @Given("Home page is opened")
    public void homePageIsOpened() {
        homePage.openHomePage();
    }

    @When("User clicks Sign in link in top menu")
    public void userClicksSignInLinkInTopMenu() {
        homePage.clickSignInLink();
    }

    @Then("Authentication page is opened")
    public void authenticationPageIsOpened() {
        authenticationPage.verifyIfAuthenticationPageIsOpened();
    }

    @And("User clicks Create an account button")
    public void userClicksCreateAnAccountButton() {
        authenticationPage.clickCreateAccountButton();
    }

    @Then("User gets a message {string}")
    public void userGetsAMessage(String expectedMessage) {
        authenticationPage.waitForErrorMessage();
        String visibleErrorMessage = authenticationPage.getErrorMessage();
        assertThat("Expected error message should be: " + expectedMessage + " but visible error message is: " + visibleErrorMessage, expectedMessage.equals(visibleErrorMessage));
    }

    @And("User fill email address field with {string}")
    public void userFillEmailAddressFieldWith(String email) {
        authenticationPage.enterEmail(email);
    }

    @When("User fill login field {string}")
    public void userFillLoginField(String email) {
        authenticationPage.fillLoginmailField(email);
    }

    @And("User fill password field {string}")
    public void userFillPasswordField(String password) {
        authenticationPage.fillPasswordField(password);
    }

    @And("User click Sign in button")
    public void userClickSignInButton() {
        authenticationPage.clickSignInButton();
    }

    @Then("Error message is visible {string}")
    public void errorMessageIsVisible(String errorMessage) {
        String visibleMessage = authenticationPage.getLoginErrorMessage();
        assertThat("Error message should be displayed", authenticationPage.isErrorMessageDisplayed());
        assertThat("Error message should be " + errorMessage + " but is " + visibleMessage, visibleMessage.contains(errorMessage));
    }

    @When("User authenticate using credentials")
    public void userAuthenticateUsingCredentials(DataTable credentials) {
        List<String> userCredentials = credentials.row(1);
        String addressEmail = userCredentials.get(0);
        String password = userCredentials.get(1);
        authenticationPage.fillLoginmailField(addressEmail);
        authenticationPage.fillPasswordField(password);
        authenticationPage.clickSignInButton();
//        authenticationPage.authenticate(addressEmail, password);
    }

    @Then("User can see My Account Page")
    public void userCanSeeMyAccountPage() {
        myAccountPage.verifyIfMyAccountPageIsOpened();
    }
}

