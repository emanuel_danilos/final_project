package com.mycompany.app.pages;

import com.mycompany.app.DriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class BasePage {
    public final RemoteWebDriver driver;

    public BasePage() {
        this.driver = DriverManager.DRIVER;
        PageFactory.initElements(this.driver, this);
    }
}


